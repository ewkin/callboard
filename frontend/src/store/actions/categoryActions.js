import axiosApi from "../../axiosApi";

export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';

export const fetchCategorySuccess = categories => ({type: FETCH_CATEGORIES_SUCCESS, categories});


export const fetchCategories = () => {
    return async dispatch => {
        const response = await axiosApi('/categories');
        dispatch(fetchCategorySuccess(response.data));
    };
};