import React, {useEffect} from 'react';
import {clearErrors} from "../../store/actions/usersActions";
import {useDispatch, useSelector} from "react-redux";
import SideBar from "../../components/SideBar/SideBar";
import {fetchCategories} from "../../store/actions/categoryActions";
import history from "../../history";
import {makeStyles} from "@material-ui/core/index";
import {clearErrorsAdds, fetchAdds} from "../../store/actions/addsActions";
import Adds from "./Adds";
import {useParams} from "react-router-dom";

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
  }
}));

const Categories = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories);
  const addsData = useSelector(state => state.adds.adds);
  const {category} = useParams();

  let adds = addsData;
  if (category) {
    adds = addsData.filter(add => {
      return add.category.title === category;
    });
  }

  useEffect(() => {
    dispatch(fetchCategories());
    dispatch(fetchAdds());

  }, [dispatch]);

  history.listen((location) => {
    if (location.pathname === '/login' || location.pathname === '/register') {
      dispatch(clearErrors());
    } else if (location.pathname === '/adds/new') {
      dispatch(clearErrorsAdds());
    }
  });

  return (
    <div className={classes.root}>
      <SideBar categories={categories}/>
      <Adds adds={adds} category={category}/>
    </div>
  );
};

export default Categories;