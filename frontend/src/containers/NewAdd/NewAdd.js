import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import AddForm from "../../components/AddForm/AddForm";
import {fetchCategories} from "../../store/actions/categoryActions";
import {createAdd} from "../../store/actions/addsActions";

const NewAdd = ({history}) => {

  const dispatch = useDispatch();
  const categories = useSelector(state => state.categories.categories)
  const user = useSelector(state => state.users.user)

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);


  const onProductsFormSubmit = async productData => {
    await dispatch(createAdd(productData, user.token));
  };


  return (
    <Grid container direction="column">
      <Grid item xs>
        <Typography variant='h4'>
          New products
        </Typography>
      </Grid>
      <Grid item xs>
        <AddForm onSubmit={onProductsFormSubmit} categories={categories}/>
      </Grid>
    </Grid>
  );
};

export default NewAdd;