import React, {useEffect} from 'react';
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch, useSelector} from "react-redux";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {useParams} from "react-router-dom";
import {CardActions, CardMedia} from "@material-ui/core/index";
import imageNotAvailable from "../../assets/images/notAvailable.png";
import {apiURL} from "../../config";
import {deleteAdd, fetchSingleAdd} from "../../store/actions/addsActions";
import Typography from "@material-ui/core/Typography/Typography";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton";


const useStyles = makeStyles(theme => ({
  progress: {
    height: theme.spacing(20)
  },
  card: {
    height: '50%'
  },
  media: {
    paddingTop: '60%',
  },
  content: {
    marginBottom: '10px'
  }
}));

const SingleAdd = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.adds.addsLoading);
  const user = useSelector(state => state.users.user);
  const add = useSelector(state => state.adds.add);
  const {id} = useParams();

  useEffect(() => {
    dispatch(fetchSingleAdd(id));
  }, [dispatch, id]);

  let cardImage = imageNotAvailable;
  if (add.image) {
    cardImage = apiURL + '/' + add.image;
  }
  let [category, seller, phone, userId] = ['', '', '', ''];
  let show = false;
  if (add.title) {
    category = add.category.title;
    seller = add.seller.name;
    userId = add.seller._id
    phone = add.seller.phone;
  }

  if (user) {
    if (user._id === userId) {
      show = true;
    }
  }


  return (
    <>

      {loading ? (
        <Grid container justify='center' alignContent="center" className={classes.progress}>
          <Grid item>
            <CircularProgress/>
          </Grid>
        </Grid>) : (
        <Grid>
          <Card className={classes.card}>
            <CardHeader title={add.title}/>
            <CardContent>
              <Grid className={classes.content}>
                <Typography variant='body2'>{add.description}</Typography>
              </Grid>
              <Grid item className={classes.content} xs={12} sm={8} md={4} lg={4}>
                <CardMedia
                  image={cardImage}
                  title={add.title}
                  className={classes.media}
                />
              </Grid>
              <Grid className={classes.content}>
                <Typography variant='body2'>
                  <strong> Category: </strong>{category}
                </Typography>
              </Grid>
              <Grid className={classes.content}>
                <Typography variant='body2'>
                  <strong> Price: </strong>{add.price} KGS
                </Typography>
              </Grid>
              <Grid className={classes.content}>
                <Typography variant='body2'>
                  <strong> Seller: </strong>{seller}
                </Typography>
              </Grid>
              <Grid className={classes.content}>
                <Typography variant='body2'>
                  <strong> Phone: </strong>{phone}
                </Typography>
              </Grid>
            </CardContent>
            {!user || !show ? null : (
              <CardActions>
                <IconButton onClick={() => dispatch(deleteAdd(id, user.token))}>
                  <DeleteIcon/>
                </IconButton>
              </CardActions>
            )}
          </Card>
        </Grid>
      )}
    </>
  );
};

export default SingleAdd;