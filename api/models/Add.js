const mongoose = require('mongoose');

const AddSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Title required']
  },
  price: {
    type: Number,
    min: [1, 'Minimum price 1 KGS'],
    required: [true, 'Price required']
  },
  description: {
    type: String,
    required: [true, 'Description required']
  },
  image: {
    type: String,
    required: [true, 'Image required']
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
    required: [true, 'Category required'],
  },
  seller: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  }
});


const Add = mongoose.model('Add', AddSchema);
module.exports = Add;