const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Add = require("../models/Add");
const User = require("../models/User");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath)
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const adds = await Add.find().populate('category', 'title');
    res.send(adds);
  } catch (e) {
    res.sendStatus(500);
  }

});

router.delete('/:id', async (req, res) => {
  try {
    const token = req.get('Authorization');
    if (!token) {
      return res.status(401).send({error: 'No token password'});
    }
    const user = await User.findOne({token});
    if (!user) {
      return res.status(401).send({error: 'Wrong token'});
    }
    const add = await Add.findOne({_id: req.params.id});
    if (JSON.stringify(user._id) !== JSON.stringify(add.seller)) {
      return res.status(401).send({error: 'Prohibited, not an owner'});
    }

    await Add.findByIdAndDelete(req.params.id);
    res.send('Got a DELETE request at ' + req.params.id);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const adds = await Add.findOne({_id: req.params.id}).populate('category', 'title').populate({
      path: 'seller',
      select: ['name', 'phone'],
    });
    if (adds) {
      res.send(adds);
    } else {
      res.sendStatus(404);
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


router.post('/', upload.single('image'), async (req, res) => {
  try {
    const token = req.get('Authorization');
    if (!token) {
      return res.status(401).send({error: 'No token password'});
    }
    const user = await User.findOne({token});
    if (!user) {
      return res.status(401).send({error: 'Wrong token'});
    }
    const addData = req.body;
    addData.seller = user._id;
    if (req.file) {
      addData.image = 'uploads/' + req.file.filename;
    }
    const add = new Add(addData);
    await add.save();
    res.send({add});
  } catch (e) {
    res.status(400).send(e);
  }

});

module.exports = router;