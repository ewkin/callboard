const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Category = require("./models/Category");
const Add = require("./models/Add");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);
  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }
  const [user, admin] = await User.create({
    username: 'user',
    password: '1qaz@WSX29',
    name: 'John',
    phone: '+996777777858',
    token: nanoid(),
  }, {
    username: 'admin',
    password: '1qaz@WSX29',
    name: 'Admin',
    phone: '+996777777858',
    token: nanoid(),
  });

  const [computers, cars, home, other] = await Category.create({
    title: 'Computers',
    description: 'Used computers'
  }, {
    title: 'Cars',
    description: 'Used cars'
  }, {
    title: 'Home',
    description: 'Home appliances'
  }, {
    title: 'Other'
  });

  await Add.create({
    title: 'Ультрабук Dell-модель-Latitude E7240-процессор-core',
    price: 24000,
    description: 'Dell Latitude E7240 является компактным 12-дюймовым ультрабуком, который за счет богатства конфигураций подойдет практически любому пользователю. При желании можно найти конфигурацию с сенсорным экраном формата Full-HD, или с мощным процессором семейства Intel Core i7.',
    category: computers,
    image: 'fixtures/DellLatitudeE7240.jpg',
    seller: user
  }, {
    title: 'Ноутбук acer i5-4пок (сост отл )-процессор-i5-4210U',
    price: 23000,
    description: 'Алюминиевый корпус производит приятное впечатление как своим внешним видом, так и на ощупь. Смесь серебристого и тёмно-серого цветов является единственным доступным в продаже окрасом, так что выбирать, в любом случае, не приходится.',
    category: computers,
    image: 'fixtures/AcerI5.jpg',
    seller: user
  }, {
    title: 'Mitsubishi Space Star 1.6 л. 2002',
    price: 210000,
    description: 'Mitsubishi Space Star — субкомпактвэн компании Mitsubishi, выпускавшийся с 1998 по 2005 год на заводе NedCar в Нидерландах. Space Star создан на одной платформе с Mitsubishi Carisma и Volvo S40/V40.',
    category: cars,
    image: 'fixtures/Car.jpeg',
    seller: admin
  }, {
    title: 'Уборка помещений | Офисы, Квартиры, Дома | Генеральная уборка, Ежедневная уборка, Уборка после ремонта',
    price: 2200,
    description: 'Клининговая компания\n' +
      'Уборка квартир\n' +
      'Уборка квартиры\n' +
      'Уборка дом\n' +
      'Уборка в доме\n' +
      ', в офисах ,\n' +
      'Уборка производственных помещений\n' +
      'после ремонта\n' +
      'Услуги клининга в Бишкеке\n' +
      'Сделаем быстро , качественно , чисто',
    category: home,
    image: 'fixtures/Cleaning.jpeg',
    seller: user
  });


  await mongoose.connection.close();

};

run().catch(console.error);